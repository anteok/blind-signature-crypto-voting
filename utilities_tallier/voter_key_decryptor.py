#!./venv/bin/python3
from Crypto.Cipher import PKCS1_OAEP
from sys import argv

"""
Скрипт для расшифрования присланного ключа избирателя.
Запуск в bash: ./voter_key_decryptor.py <шифровка> <файл приватного ключа> <имя для расшифрованного файла>
"""

def decrypt_by_private_key(byte_encrypted, byte_private_key):

    cipherrsa = PKCS1_OAEP.new(byte_private_key)
    return bytes(cipherrsa.decrypt(byte_encrypted))


if __name__ == '__main__':
    if len(argv) != 3 or len(argv) != 4:
        print('Usage: {} <encrypted> <your_privatekey> <filename>'.format(argv[0]))
    else:
        with open(argv[1]) as f:
            encrypted = f.read()
        with open(argv[2]) as f:
            private_key = f.read()
        filename = argv[3] if len(argv == 4) else 'publ.txt'
        with open(filename, 'w') as f:
            f.write(
                decrypt_by_private_key(
                    byte_encrypted=encrypted.encode(),
                    byte_private_key=private_key.encode()
                ).decode()
            )
