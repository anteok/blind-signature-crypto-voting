#!./venv/bin/python3

import json
from sys import argv
import os
from glob import glob

"""
Скрипт для генерации списка избирателей.
Запуск в bash: ./voters_list_generator <директория с публичными ключами избирателей>
"""
def generate_voters_list(directory):
    voters = {}
    for file_key in glob(os.path.join(directory + 'publ*.txt')):
        voter_number = file_key.split('/')[-1][4:-4]
        with open(file_key) as f:
            voters["voter_{}".format(voter_number)] = f.read()
    with open(directory + 'voters_list.json', 'w') as f:
        json.dump(voters, f)


if __name__ == '__main__':
    if len(argv) > 2:
        print('Usage: {} <directory>'.format(argv[0]))
    elif len(argv) == 2:
        if argv[1][-1] == '/':
            generate_voters_list(argv[1])
        else:
            generate_voters_list(argv[1] + '/')
    else:
        generate_voters_list('./')
