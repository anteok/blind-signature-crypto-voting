#!./venv/bin/python3

from Crypto.PublicKey import RSA
from sys import argv

"""
Скрипт для генерации пар ключей.
Запуск в bash: ./key_generator.py <число пар> <директория, куда сохранять>
"""
def generate_key_pairs(count, directory):
    for i in range(count):
        with open('{}privl{}.txt'.format(directory, i), 'wb') as fpriv:
            priv = RSA.generate(2048)
            fpriv.write(bytes(priv.exportKey('PEM')))
        with open('{}publ{}.txt'.format(directory, i), 'wb') as fpubl:
            publ = priv.publickey()
            fpubl.write(bytes(publ.exportKey('PEM')))


if __name__ == '__main__':
    if len(argv) > 3:
        print('Usage: {} <int count> <path_to_store_keys>'.format(argv[0]))
    elif len(argv) == 3:
        if argv[2][-1] == '/':
            generate_key_pairs(int(argv[1]), argv[2])
        else:
            generate_key_pairs(int(argv[1]), argv[2] + '/')
    else:
        generate_key_pairs(argv[1], './')
