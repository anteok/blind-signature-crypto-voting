#!./venv/bin/python3

from sys import argv
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
"""
Скрипт для проверки голосов.
Запуск в bash: ./check_vote.py <файл приватного ключа> <текст голоса>
"""

def decrypt_by_private_key(byte_encrypted, byte_private_key):
    private_key = RSA.importKey(byte_private_key)
    cipherrsa = PKCS1_OAEP.new(private_key)
    return cipherrsa.decrypt(byte_encrypted)


def check_vote(file_private_key, encrypted):
    byte_encr = bytes.fromhex(encrypted)
    try:
        _ = decrypt_by_private_key(byte_encr, open(file_private_key, 'rb').read())
        return True
    except ValueError:
        return False


if __name__ == '__main__':
    if len(argv) != 3:
        print('Usage: {} <file_private_key> <encrypted_text>'.format(argv[0]))
    else:
        if check_vote(argv[1], argv[2]):
            print('Это ваш голос')
        else:
            print('Это не ваш голос')
