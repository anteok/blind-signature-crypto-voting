#!./venv/bin/python3
from Crypto.Cipher import PKCS1_OAEP
from sys import argv

"""
Скрипт для шифрования ключа избирателя.
Запуск в bash: ./voter_key_encryptor.py <файл своего публичного ключа> <файл публичного ключа ЦИКа> 
"""

def encrypt_by_public_key(byte_text, byte_public_key):

    cipherrsa = PKCS1_OAEP.new(byte_public_key)
    return bytes(cipherrsa.encrypt(byte_text))


if __name__ == '__main__':
    if len(argv) != 3:
        print('Usage: {} <your_pubkey> <tallier\'s_pubkey>'.format(argv[0]))
    else:
        with open(argv[1]) as f:
            own_key = f.read()
        with open(argv[2]) as f:
            tallier_key = f.read()
        with open('encrypted', 'w') as f:
            f.write(
                encrypt_by_public_key(
                    byte_text=own_key.encode(),
                    byte_public_key=tallier_key.encode()
                ).decode()
            )
