"""
Классы, отвечающие за создание простых элементов веб-страниц
"""
from flask_wtf import FlaskForm
from wtforms import SubmitField
from wtforms.validators import DataRequired
from flask_wtf.file import FileField


class MainTallierForm(FlaskForm):
    """
        Стартовая страница ЦИКа, поля для добавления файлов.
    """
    file_voters_list = FileField(validators=[DataRequired()], label='Список избирателей')
    file_candidates_list = FileField(validators=[DataRequired()], label='Список кандидатов')
    # time_field = DateTimeField(validators=[])
    submit = SubmitField('Начать выборы')
