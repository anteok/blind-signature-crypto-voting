"""
Методы для маршрутизации страниц.
"""
from flask import render_template, redirect, url_for, request
from flask_login import current_user, login_required

from app import current_app
from app.main.additional_functions import register_voters_to_db, register_candidates
from app.main import bp
from app.main.forms import MainTallierForm


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    """
    Стартовая страница.
    :return:
    """
    form = None
    if current_user.username == 'tallier' and not current_app.timer.is_alive():
        form = MainTallierForm()

        if form.validate_on_submit():

            voters_file = request.files[form.file_voters_list.name].read()
            register_voters_to_db(voters_file)

            candidates_file = request.files[form.file_candidates_list.name].read()
            register_candidates(candidates_file)

            current_app.timer.start()

            return redirect(url_for('main.index'))
        elif current_app.timer.is_alive():  # TODO: разобраться, почему после начала выборов выкидывает на первую страницу
            return redirect(url_for('voting.resume'))
    return render_template('index.html', user=current_user, form=form, app=current_app)
