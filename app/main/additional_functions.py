import json
from app.models import User, Candidate
from app import db
import csv


def register_voters_to_db(voters_file):
    """
    Метод для регистрации в БД учетных записей из загруженного файла
    :param voters_file: json-файл с данными избирателей
    :return:
    """
    data = json.loads(voters_file.decode())
    for voter in data:
        user = User()
        user.username = voter
        user.public_key = data[voter]
        db.session.add(user)
        db.session.commit()


def register_candidates(candidates_file):
    """
    Метод для регистрации в БД кандидатов
    :param candidates_file: csv-файл с данными о кандидатах
    :return:
    """
    candidates_dict = csv.DictReader(candidates_file.decode().splitlines())
    for candid in candidates_dict:
        new_candid = Candidate()
        new_candid.name = candid['name']
        new_candid.status = candid['status']
        new_candid.department = candid['department']
        db.session.add(new_candid)
        db.session.commit()
