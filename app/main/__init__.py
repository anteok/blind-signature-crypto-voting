"""
Blueprint - отдельный модуль кода, выполняющий определенную задачу.
Это основной модуль, в нем расположена вся логика.
"""
from flask import Blueprint

bp = Blueprint('main', __name__)

from app.main import routes
