"""
Blueprint - отдельный модуль кода, выполняющий определенную задачу.
Задача этого модуля - авторизация.
"""

from flask import Blueprint

bp = Blueprint('auth', __name__)

from app.auth import routes
