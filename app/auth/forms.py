"""
Классы, отвечающие за создание простых элементов веб-страниц
"""
from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, FileField
from wtforms import PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, EqualTo


class LoginTallierForm(FlaskForm):
    """
    Форма логина для ЦИКа
    """
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')


class LoginVoterForm(FlaskForm):
    """
    Форма логина для избирателя
    """
    file_key = FileField('Публичный ключ', validators=[FileRequired()])
    submit = SubmitField('Войти')


class RegistrationTallierForm(FlaskForm):
    """
    Форма регистрации ЦИКа
    """
    password = PasswordField('Пароль', validators=[DataRequired()])
    password2 = PasswordField(
        'Повторите пароль', validators=[DataRequired(), EqualTo('password')]
    )
    submit = SubmitField('Зарегистрироваться')
