"""
Методы для маршрутизации страниц.
"""
from flask import render_template, redirect, url_for, flash, request
from flask_login import login_user, logout_user, current_user
from app.auth import bp
from app.auth.forms import LoginTallierForm, RegistrationTallierForm, LoginVoterForm
from app.models import User


@bp.route('/login', methods=['GET', 'POST'])
def login():
    """
    Страница /login, основная страница логина
    """
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    return render_template(
        template_name_or_list='auth/login.html',
        title='Войти',
        tallier_registered=User.is_tallier_registered()
    )


@bp.route('/login_voter', methods=['GET', 'POST'])
def login_voter():
    """
    Страница /login_voter, страница логина для избирателя
    """
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginVoterForm()
    if form.validate_on_submit():
        file = request.files[form.file_key.name].read()
        user = User.query.filter_by(public_key=file.decode()).first()
        print(user)
        if user is None:
            flash('Вы не зарегистрированы для участия в выборах либо ключ неверен')
            return redirect(url_for('auth.login_voter'))
        login_user(user)
        return redirect(url_for('main.index'))
    return render_template('auth/login_voter.html', title='Войти как избиратель', form=form)


@bp.route('/login_tallier', methods=['GET', 'POST'])
def login_tallier():
    """
    Страница /login_tallier, страницв логина ЦИКа
    """
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginTallierForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username='tallier').first()
        if user is None or not user.check_password(form.password.data):
            flash('Неверный логин или пароль')
            return redirect(url_for('auth.login_tallier'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('main.index'))
    return render_template('auth/login_tallier.html', title='Войти', form=form)


@bp.route('/logout')
def logout():
    """
    Страница /logout
    """
    logout_user()
    return redirect(url_for('main.index'))


@bp.route('/register_tallier', methods=['GET', 'POST'])
def register_tallier():
    """
    Страница /register_tallier
    """
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationTallierForm()
    if form.validate_on_submit():
        User.new_user(form)
        flash('Поздравляем, вы теперь зарегистрированы!')
        return redirect(url_for('auth.login'))
    return render_template('auth/register_tallier.html', title='Регистрация',
                           form=form)
