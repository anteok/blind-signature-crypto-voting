from Crypto.Hash import SHA384
from Crypto.Signature import PKCS1_v1_5
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from random import SystemRandom


def generate_asymmetric_keys():
    """
    Генерация пары публичный-приватный ключ.
    :return: (private_key, public_key)
    """
    _private_key = RSA.generate(2048)
    _public_key = _private_key.publickey()
    rand_num = SystemRandom().randrange(_public_key.n >> 10, _public_key.n)
    return _private_key.exportKey('PEM').decode(), _public_key.exportKey('PEM').decode(), str(rand_num)


def sign_text(byte_text, byte_private_key):
    """
    Подписывает текст приватным ключом.
    :param byte_text: исходный текст
    :param byte_private_key: приватный ключ
    :return: signature
    """
    signature = PKCS1_v1_5.new(byte_private_key)
    return signature.sign(SHA384.new(byte_text))


def if_signature_valid(byte_text, signature, byte_public_key):
    """
    Проверка подписи.
    :param byte_text: исходный текст
    :param signature: подпись
    :param byte_public_key: публичный ключ
    :return:
    """
    public_sign = PKCS1_v1_5.new(byte_public_key)
    return public_sign.verify(SHA384.new(byte_text), signature)


def encrypt_by_public_key(byte_text, byte_public_key):
    """
    Шифрование публичным ключом
    :param byte_text: исходный текст
    :param byte_public_key: публичный ключ
    :return: шифровка
    """
    public_key = RSA.importKey(byte_public_key)
    cipherrsa = PKCS1_OAEP.new(public_key)
    return cipherrsa.encrypt(byte_text)


def decrypt_by_private_key(byte_encrypted, byte_private_key):
    """
    Расшифрование приватным ключом
    :param byte_encrypted: шифровка
    :param byte_private_key: приватный ключ
    :return: расшифрованный текст
    """
    private_key = RSA.importKey(byte_private_key)
    cipherrsa = PKCS1_OAEP.new(private_key)
    return cipherrsa.decrypt(byte_encrypted)
