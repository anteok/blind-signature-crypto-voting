"""
Blueprint - отдельный модуль кода, выполняющий определенную задачу.
Задача этого модуля - криптографические функции.
"""
from flask import Blueprint

bp = Blueprint('crypto', __name__)
