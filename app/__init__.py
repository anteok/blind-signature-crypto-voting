"""
Основной инициализирующий скрипт приложения
подключение модулей (blueprints)
чтение файла конфигурации
"""
import logging
from logging.handlers import RotatingFileHandler
import os
from flask import Flask, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from threading import Timer

from config import Config

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
login.login_view = 'auth.login'
login.login_message = 'Please log in to access this page.'
bootstrap = Bootstrap()
moment = Moment()


def on_timer_function(any_app):
    """
    Функция, исполняющаяся по истечению таймера.
    :param any_app: ссылка на объект приложения
    :return:
    """
    any_app.voting_finished = True
    print('-- VOTING FINISHED --')


def create_app(config_class=Config):
    """
    Функция, создающая объект приложения
    :param config_class: объект, содержащий параметры конфигурации
    :return:
    """
    app = Flask(__name__)
    app.config.from_object(config_class)

    # Инициализация основных модулей
    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    bootstrap.init_app(app)
    moment.init_app(app)

    # Подключение blueprints
    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    from app.voting import bp as voting_bp
    app.register_blueprint(voting_bp)

    # Инициализация таймера
    app.voting_finished = False
    app.timer = Timer(
        interval=app.config['VOTING_TIME'],
        function=on_timer_function,
        args=[app]
    )

    # Настройки логирования
    if not app.debug and not app.testing:

        if app.config['LOG_TO_STDOUT']:
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(logging.INFO)
            app.logger.addHandler(stream_handler)
        else:
            if not os.path.exists('logs'):
                os.mkdir('logs')
            file_handler = RotatingFileHandler('logs/cryptovoting.log',
                                               maxBytes=10240, backupCount=10)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(levelname)s: %(message)s '
                '[in %(pathname)s:%(lineno)d]'))
            file_handler.setLevel(logging.INFO)
            app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('CryptoVoting startup')

    return app


from app import models
