"""
Модели - весь backend приложения. Работа с БД.
Для работы с SQLite используется библиотека SQLAlchemy, позволяющая
работать с БД, обращаясь к сущностям как к объектам (ORM).
"""
from flask_login import UserMixin, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from app.crypto.cryptography import encrypt_by_public_key, generate_asymmetric_keys

from app import db, login


class User(UserMixin, db.Model):
    """
    Класс, описывающий объект пользователя сервиса
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    private_key = db.Column(db.String(2000), unique=True)
    public_key = db.Column(db.String(512), index=True, unique=True)
    has_voted = db.Column(db.Boolean, default=False)
    random_number = db.Column(db.String(1024))

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        """
        Задать пароль пользователю
        :param password: строка
        :return:
        """
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        """
        Проверка пароля пользователя
        :param password:
        :return:
        """
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def new_user(form):
        """
        Добавление нового пользоввтеля в БД
        :param form:
        :return:
        """
        user = User()
        user.username = 'tallier'
        user.set_password(form.password.data)
        user.private_key, user.public_key, user.random_number = generate_asymmetric_keys()
        db.session.add(user)
        db.session.commit()

    @staticmethod
    def get_tallier_pubkey():
        """
        Получить из базы публичный ключ ЦИКа
        :return:
        """
        tallier = User.query.filter_by(username='tallier').first()
        if tallier:
            return tallier.public_key

    @staticmethod
    def is_tallier_registered():
        """
        Проверка, зарегистрирован ли ЦИК
        :return:
        """
        return True if User.query.count() else False


class Candidate(db.Model):
    """
    Класс, описывающий кандидата.
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=True)
    status = db.Column(db.String(128))
    department = db.Column(db.String(256))

    def __repr__(self):
        return '<Candidate {}>'.format(self.name)

    @staticmethod
    def get_choices():
        """
        Получить из базы ID и имена кандидатов
        :return:
        """
        return [(str(i.id), i.name) for i in Candidate.query.all()]

    @staticmethod
    def get_candidates_list():
        """
        Получить список кандидатов
        :return:
        """
        candidates = list()
        for candid in Candidate.query.all():
            candidates.append(
                {
                    'id': candid.id,
                    'name': candid.name,
                    'status': candid.status,
                    'department': candid.department
                }
            )
        return candidates


class Vote(db.Model):
    """
    Класс, описывающий голоса
    """
    id = db.Column(db.Integer, primary_key=True)
    encrypted = db.Column(db.String(512))

    candidate_id = db.Column(db.Integer, db.ForeignKey('candidate.id'))
    candidate = db.relationship('Candidate')

    def __repr__(self):
        return '<Vote for {}>'.format(self.candidate_id)


def pull_vote(votes_list, voter_public_key):
    """
    Запрос на добавление голоса в базу
    :param votes_list:
    :param voter_public_key:
    :return:
    """
    for voted_id in votes_list:
        vote = Vote()
        vote.candidate_id = int(voted_id)
        vote.encrypted = encrypt_by_public_key(voted_id.encode(), voter_public_key.encode()).hex()

        current_user.has_voted = True

        db.session.add(vote)
        db.session.commit()


def get_votes_for_candidate(name):
    """
    Запрос на получение голосов за конкретного избирателя
    :param name:
    :return:
    """
    return [
        i[1] for i in Candidate.query.join(
            Vote, Vote.candidate_id == Candidate.id
        ).add_columns(
            Vote.encrypted
        ).filter(
            Candidate.name == name
        )
    ]


def get_count_users():
    """
    Запрос числа зарегистрированных пользователей
    :return:
    """
    return User.query.count()


def get_voted_users_count():
    """
    Запрос количества проголосовавших избирателей
    :return:
    """
    return User.query.filter(User.has_voted).count()


def get_percentage_votes(name):
    """
    Функция для расчета процентного соотношения голосов
    :param name:
    :return:
    """
    return round(len(get_votes_for_candidate(name)) / (get_voted_users_count()) * 100, 2)


@login.user_loader
def load_user(_id):
    """
    Callback для получения пользователя по id. Нужен для работы пользовательских сессий.
    :param _id:
    :return:
    """
    return User.query.get(int(_id))
