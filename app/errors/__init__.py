"""
Blueprint - отдельный модуль кода, выполняющий определенную задачу.
Задача этого модуля - обработка ошибочных запросов.
"""
from flask import Blueprint

bp = Blueprint('errors', __name__)

from app.errors import handlers
