from flask import render_template
from app import db, current_app
from app.errors import bp


@bp.app_errorhandler(404)
def not_found_error():
    """
    Обработчик кода 404
    :return:
    """
    return render_template('errors/404.html', app=current_app), 404


@bp.app_errorhandler(500)
def internal_error():
    """
    Обработчик кода 500
    :return:
    """
    db.session.rollback()
    return render_template('errors/500.html', app=current_app), 500
