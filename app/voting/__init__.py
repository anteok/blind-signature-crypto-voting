from flask import Blueprint

bp = Blueprint('voting', __name__)

from app.voting import routes