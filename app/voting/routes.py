from app.voting import bp
from app.voting.forms import BulletinForm
from flask_login import login_required, current_user
from flask import render_template, redirect, url_for, request
from app.models import Candidate, pull_vote, get_votes_for_candidate, get_percentage_votes, get_voted_users_count, \
    get_count_users
from pytils.dt import ru_strftime
from datetime import datetime
from app import current_app, on_timer_function


@bp.route('/bulletin', methods=['GET', 'POST'])
@login_required
def bulletin():

    if current_app.voting_finished:
        return redirect(url_for('main.index'))

    if current_user.has_voted:
        return redirect(url_for('voting.already_voted'))

    form = BulletinForm()
    form.candidates_selector.choices = Candidate.get_choices()

    ru_date = ru_strftime("%d %B %Y", inflected=True, date=datetime.now())

    if form.validate_on_submit():
        pull_vote(
            votes_list=request.form.getlist('candidates_selector'),
            voter_public_key=current_user.public_key
        )
        if get_count_users() - get_voted_users_count() == 1:
            current_app.timer.cancel()
            on_timer_function(current_app)
        return redirect(url_for('voting.resume'))

    return render_template(
        template_name_or_list='voting/bulletin.html',
        title="Бюллетень голосования",
        form=form,
        candidates=Candidate.get_candidates_list(),
        ru_date=ru_date,
        app=current_app,
    )


@bp.route('/resume')
@login_required
def resume():
    return render_template('voting/resume.html', app=current_app, current_user=current_user)


@bp.route('/already_voted')
@login_required
def already_voted():
    if current_user.username == 'tallier':
        return redirect(url_for('main.index'))
    return render_template('voting/already_voted.html', app=current_app)


@bp.route('/results')
@login_required
def results():
    if not current_app.voting_finished:
        return redirect(url_for('main.index'))
    candidates = Candidate.get_candidates_list()
    voted_users_count = get_voted_users_count()

    votes_dict = {
        i['name']: get_votes_for_candidate(i['name']) for i in candidates
    }

    percentage = {
        i['name']: get_percentage_votes(i['name']) for i in candidates
    }
    return render_template(
        template_name_or_list='voting/results.html',
        votes_dict=votes_dict,
        percentage=percentage,
        app=current_app,
        count_voted=voted_users_count,
    )
