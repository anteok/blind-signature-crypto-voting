from flask_wtf import FlaskForm
from wtforms import SelectMultipleField, SubmitField
from wtforms.widgets import ListWidget, HTMLString, html_params
from flask import escape


class SelectCheckbox(object):

    def __call__(self, field, **kwargs):
        html = ['']
        for val, label, selected in field.iter_choices():
            html.append(self.render_option(field.name, val, label, selected))
        return HTMLString(u''.join(html))

    @classmethod
    def render_option(cls, name, value, label, selected):
        options = {'value': value}
        if selected:
            options['checked'] = u'checked'
        return HTMLString(
            u'<br><input type="checkbox" name="%s" %s>   %s</input>' % (name, html_params(**options), escape(label))
        )


class BulletinForm(FlaskForm):
    candidates_selector = SelectMultipleField(
        widget=SelectCheckbox(),
        option_widget=ListWidget(prefix_label=''),
        label='Ваш выбор',
        choices=[],
    )
    submit = SubmitField('Выбрать')



