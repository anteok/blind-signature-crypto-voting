Сервис для голосования.

Установка:

cd <папка с проектами>\
sudo apt-get update\
sudo apt-get upgrade\
sudo apt-get install python3-pip git python3-venv\
git clone https://anteok@bitbucket.org/anteok/blind-signature-crypto-voting.git\
cd ./blind-signature-crypto-voting \
python3 -m venv ./venv 
source /venv/bin/activate
pip3 install -r requirements.txt

Запуск здесь же: \
source /venv/bin/activate \
flask run --cert cert.pem --key key.pem

После этого сервер запустится и будет работать. На сайт можно зайти по адресу http://localhost:5000

Обновление репозитория до актуального:

cd <папка с проектами>/blind-signature-crypto-voting \
git pull origin master

Создание сертификата и приватного ключа для https:

openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365

готовый сертификат и ключ уже лежат в корневой папке.

ПОРЯДОК ПРЕЗЕНТАЦИИ:
1. Запустили в консоли сервер приложения.
2. Убедились, что база пустая (на странице авторизации иожно зарегистрироваться как ЦИК).
Если нет, то ./workers/flush_db.sh
Если скрипт не запускается таким образом, дать права на запуск chmod +x ./workers/flush_db.sh.
3. Регистрируемся как ЦИК, запоминаем свой пароль.
4. Генерируем ключи, списки и прочее, складываем куда-то всё в одну папку. В папке test_files есть пример документов, которые надо нагенерировать.
5. Входим как ЦИК, подгружаем два списка. После того, как будет нажата кнопка, голосование начнется. 
6. Можем разлогиниваться как ЦИК, заходим как избиратели по ключу, голосуем.
7. По истечении времени в консоль вывалится сообщение об окончании, после этого каждый пользователь (и ЦИК) может зайти и ознакомиться с результатами голосования.
Profit.
