#!/bin/bash

# Скрипт для очищения базы данных.

rm ./migrations/versions/*
rm app.db
flask db migrate -m "new"
flask db upgrade
