"""
Инициализация приложения
"""
from app import create_app
from app.models import User, Vote, Candidate

app = create_app()


@app.shell_context_processor
def make_shell_context():
    """
    Добавление во встроенную командную оболочку объектов БД по умолчанию
    :return:
    """
    return {
        'db': app.db,
        'User': User,
        'app': app,
        'Vote': Vote,
        'Candidate': Candidate
    }
